const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const keys = require('../config/keys');
const mongoose = require('mongoose');
const User = mongoose.model('users');

passport.use(new FacebookStrategy({
        clientID: keys.facebookClientID,
        clientSecret: keys.facebookClientSecret,
        callbackURL: "/auth/facebook/callback",
        profileFields:['displayName', 'email', 'gender', 'cover',],
        proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
        const generateUser = async (id,name, email) => {
            const user = await new User({facebookID: id, name:name, email:email}).save();
            done(null, user);
        };
        const existingUser = await User.findOne({facebookID: profile.id,});
        existingUser ? done(null, existingUser) : generateUser(profile.id, profile.displayName, profile.emails[0].value);
    }
));

passport.use(new GoogleStrategy({
        clientID: keys.googleClientID,
        clientSecret: keys.googleClientSecret,
        callbackURL: '/auth/google/callback',
        proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
        const generateUser = async (id) => {
            const user = await new User({googleID:id,}).save();
            done(null, user);
        };
        const existingUser = await User.findOne({googleID: profile.id,});
        existingUser ? done(null, existingUser) : generateUser(profile.id);
    }));


passport.serializeUser((user, done) => {
    done(null, user.id)});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {done(null, user)})
});




