const mongoose = require('mongoose');
const AppKeySchema = mongoose.model('appKeys');

const facebookValues = {
    id: '',
    secretKey: '',
};

module.exports = {
    getvals: new Promise((resolve,reject)=> {
        const getValues= () => {
            AppKeySchema.find({
                $or: [
                    {
                        productName: 'facebook'
                    }
                ]
            }).then((product) => {
                facebookValues.id = product[0].productID.toString();
                facebookValues.secretKey = product[0].secretKey.toString();
                resolve(facebookValues);

            }).catch(() => {
                console.log('error');
            })
        };
        getValues();
    }),

    facebookValues: {
        id: facebookValues.id,
        secretKey: facebookValues.secretKey
    }

}
