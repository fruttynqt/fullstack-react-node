// https://rallycoding.herokuapp.com/api/music_albums
// ajax request sample

// const fetchAlbums = () => {
//     fetch('https://rallycoding.herokuapp.com/api/music_albums')
//         .then(res => res.json())
//         .then(json=>console.log(json));
// }
//

const fetchAlbums = async () => {
    const res = await fetch('https://rallycoding.herokuapp.com/api/music_albums');
    const json = await res.json();
    console.log(json);
}

fetchAlbums();



    const resolvePromise = ()=> {
        return new Promise((resolve,reject)=> {
            setTimeout(()=> {
                resolve('Hi!')
            },2000)
        })
    }
    const resolvePromise2 = (res)=> {
        return new Promise((resolve,reject)=> {
            setTimeout(()=> {
                resolve(res + ' Honey!')
            },2000)
        })
    }

    const getResult = async()=> {
        const getPromise = await resolvePromise();
        const getV2 = await resolvePromise2(getPromise);
        console.log(getV2);
    }