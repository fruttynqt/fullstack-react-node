import React, {Component} from 'react';
import {connect} from 'react-redux';

export class Credits extends Component {
    renderContent() {
        switch (this.props.credits){
            case !this.props.credits:
                return 'no';
            default:
                return this.props.credits;
        }
    }
    render() {
        return (
            <div style={{margin: '0 10px'}}>
                Credits: {this.renderContent()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        credits:state.auth.credits
    }
}
export default connect(mapStateToProps)(Credits);