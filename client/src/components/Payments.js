import React, {Component} from 'react';
import StripeCheckout from "react-stripe-checkout";
import { connect } from 'react-redux';
import * as actions from'../actions';

class Payments extends Component {
    render() {
        return (
            <StripeCheckout amount={500}
                            token= {token=>this.props.handleStripeToken(token)}
                            stripeKey={process.env.REACT_APP_STRIPE_KEY}
                            name="Emaily"
                            description="Credits payment">
                <button className="btn">Add credits</button>
            </StripeCheckout>
        )
    }
}


export default connect(null,actions)(Payments);