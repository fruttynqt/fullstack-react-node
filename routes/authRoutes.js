const passport = require('passport');

module.exports = app => {


    /**
     * facebook Routes
     */
    app.get('/auth/facebook',
        passport.authenticate('facebook', {
            scope:['public_profile', 'email']
        }));

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: '/auth/facebook' }),
        (req, res) =>{
            res.redirect('/api/current_user');
        });


    app.get(
        '/auth/google',
        passport.authenticate('google', {
            scope: ['profile', 'email']
        })
    );

    app.get(
        '/auth/google/callback',
        passport.authenticate('google'),
        (req,res)=>{
            res.redirect('/surveys')
        }
    )


    app.get(
        '/api/current_user',
        (req,res)=> {
            res.send(req.user);
        }
    );
    app.get(
        '/api/logout',
        (req,res)=> {
            console.log('logouted');
            req.logout();
            res.redirect('/');
        }
    )
};


